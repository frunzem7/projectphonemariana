import java.util.ArrayList;
import java.util.List;

public class Phone1 {
    private String number;
    private String model;
    private int weight;
    private String name;

    public Phone1() {

    }

    public Phone1(String number, String model) {
        this.number = number;
        this.model = model;
    }

    public Phone1(String number, String model, int weight) {
        new Phone1(number, model);
        this.weight = weight;
    }

    public Phone1(String number, String model, int weight, String name) {
        this.number = number;
        this.model = model;
        this.weight = weight;
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "number='" + number + '\'' +
                ", model='" + model + '\'' +
                ", weight=" + weight +
                ", name='" + name + '\'' +
                '}';
    }

    public void receiveCall(String name) {
        System.out.println("{" + name + "} is calling");
    }

    public Phone1 receiveCall(Phone1 phone) {
        System.out.println("{" + phone.getName() + "} is calling");
        System.out.println(" Number: " + phone.getNumber());
        return phone;
    }

    public void sendMessage(String phoneNumber, String message) {
        System.out.println("From " + phoneNumber + " Message: " + message);
    }

    public static void main(String[] args) {
        List<Phone1> phones = new ArrayList<>();
        phones.add(new Phone1("078945612", "Apple", 325, "Shrek"));
        phones.add(new Phone1("078945613", "Samsung", 300, "Paramon"));
        phones.add(new Phone1("078945614", "Nokia", 400, "Pikachu"));

        System.out.println(" c)");
        for (Phone1 phone1 : phones) {
            System.out.println(phone1);
        }

        System.out.println(" d)");
        for (Phone1 phone1 : phones) {
            phone1.receiveCall(phones.get(0).name);
        }

        System.out.println(" i)");
        for (Phone1 phone1 : phones) {
            phone1.receiveCall(phones.get(0));
        }

        System.out.println(" j)");
        for (Phone1 phone1 : phones) {
            phone1.sendMessage(phone1.getNumber(), "Hello");
        }
    }
}