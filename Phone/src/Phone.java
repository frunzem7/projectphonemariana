public class Phone {
    private long number;
    String model = "Samsung";
    double weight = 200.6;


    public static void main(String[] args) {

        Phone object1 = new Phone();
        System.out.println("Numarul: " + object1.number);

        Phone object2 = new Phone();
        System.out.println("Modelul telefonului: " + object2.model);

        Phone object3 = new Phone();
        System.out.println("Greutatea telefonului: " + object3.weight);
        System.out.println();

        System.out.println("Primeste apelul de la: " + receiveCall("Mariana"));
        System.out.println(receiveCall("Frunze Mariana ", 37377135));
        object1.setNumber(37377141);
        System.out.println("Numarul apelantului 1: " + object1.getNumber());
        System.out.println();

        System.out.println("Primeste apelul de la: " + receiveCall("Andrei"));
        System.out.println(receiveCall("Mircea Andrei ", 37377136));
        object2.setNumber(37377242);
        System.out.println("Numarul apelantului 2: " + object2.getNumber());
        System.out.println();

        System.out.println("Primeste apelul de la: " + receiveCall("Violeta"));
        System.out.println(receiveCall("Codreanu Violeta ", 37377137));
        object3.setNumber(37377343);
        System.out.println("Numarul apelantului 3: " + object3.getNumber());
    }

    //Getter
    public long getNumber() {
        return number;
    }

    //Setter
    public void setNumber(int newNumber) {
        number = newNumber;
    }

    //Overloading method with 1 parameter
    static String receiveCall(String nameApelant) {
        System.out.println(nameApelant + " is calling.");
        return nameApelant;
    }

    //Overloading method with 2 parameters
    static String receiveCall(String firstLastName, int number) {
        System.out.println("Numele: " + firstLastName + number);
        return firstLastName;
    }

    //Constructor with 3 parameters
    public Phone(long number, String model, double weight) {
        this(37377678, "iPhone");
        this.weight = weight;
    }

    //Constructor with 2 parameters
    public Phone(long number, String model) {
        this.number = number;
        this.model = model;
    }

    //Constructor without parameters
    public Phone() {

    }

    public void sendMessage(int numberPhone, String message){
        System.out.println("From " + numberPhone + " Message: " + message);
    }

}
